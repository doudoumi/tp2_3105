CC = g++
SOURCE_DIR = src
BUILD_DIR = bin
OBJS = $(patsubst $(SOURCE_DIR)/%.cpp,$(BUILD_DIR)/%.o,$(wildcard $(SOURCE_DIR)/*.cpp))
CPPFLAGS =   -g -std=c++11 -W  -Wall
EXEC = tp2

$(EXEC): $(OBJS)
	mkdir -p $(BUILD_DIR)
	$(CC) $^ $(LDFLAGS) -o $(EXEC)

$(BUILD_DIR)/%.o: $(SOURCE_DIR)/%.cpp $(BUILD_DIR)
	$(CC) $< $(CFLAGS) -c -o $@

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)

.PHONY: clean doc

clean:
	rm -rf $(BUILD_DIR)
doc:
	cat Readme.md

