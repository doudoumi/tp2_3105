#ifndef INTERACTION_H
#define INTERACTION_H

#include "traitement.h"

////////Fonction privé regardé le fichier .cpp//////////
void interaction(ArbreMap<string, string> &type_dico, ArbreMap<string, vector<string> > &foncteur_dico);

////////Fonction privé regardé le fichier .cpp//////////
void commande_lignes_traitement(string const &commande_ligne, ArbreMap<string, string> &type_dico, ArbreMap<string, vector<string> > &foncteur_dico);

////////Fonction privé regardé le fichier .cpp//////////
void traitement_close_foncteur(string const &commande_ligne, vector<string> const &close_foncteur,string id_foncteur,int position_variable);

////////Fonction privé regardé le fichier .cpp//////////
string partie_droite(string const &ligne, int position_variable);

////////Fonction privé regardé le fichier .cpp//////////
string partie_gauche(string const &ligne, int position_variable,string id_foncteur);

////////Fonction privé regardé le fichier .cpp//////////
string partie_gauche(string const &ligne, int position_variable);

////////Fonction privé regardé le fichier .cpp//////////
int traitement_variable(string const &ligne);
#endif
