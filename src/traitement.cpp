#include "traitement.h"

using namespace std;
/*
*Éffectue le traitement nécessaire à la creation des 2 bases de données.
*
*@param Fichier_lignes Vector<string>,un vecteur de string qui contient toute les lignes du fichier.
*@param type_dico ArbreMap<string,string>,une structure de données qui utilise l'identificateur de type comme clé et une séquence d’identificateurs représentants les éléments possibles pour ce type comme élément.
*@param foncteur_dico ArbreMap<string,vector<string>>,une structure de données qui utilise l'identificateur du foncteur comme clé et ses closes comme arguments.
*/
void pre_traitement_line(vector<string> const &Fichier_lignes, ArbreMap<string, string> &type_dico, ArbreMap<string, vector<string> > &foncteur_dico)
{
    string id_foncteur, id_type, arg_type, test;
    vector<string> close_foncteur;
    for (int i = 0; i < Fichier_lignes.size(); ++i)
    {
        if (Fichier_lignes.at(i).find("foncteur") != -1)
        {
            id_foncteur = identificateur_foncteur(Fichier_lignes.at(i));
            ++i;
            while ((Fichier_lignes.at(i).find(')') != -1) && (i < Fichier_lignes.size()))
            {
                close_foncteur.push_back(Fichier_lignes.at(i));
                ++i;
            }
            if (!foncteur_dico.contient(id_foncteur))
            {
                foncteur_dico[id_foncteur] = close_foncteur;
            }
            close_foncteur.clear();
            --i;
        }
        if (Fichier_lignes.at(i).find("type") != -1)
        {
            id_type = identificateur_type(Fichier_lignes.at(i));
            arg_type = traitement_arguments_type(Fichier_lignes.at(i));
            if (!type_dico.contient(id_type))
            {
                type_dico[id_type] = arg_type;
            }
        }
    }
}
/*
*Retourne l'identificateur du foncteur de la ligne courante.
*
* @param ligne string,la ligne d'une declaration de foncteur.
* @return id_foncteur string,l'identificateur du type.
*/
string identificateur_foncteur(string const &ligne)
{
    string id_foncteur;
    int foncteur = ligne.find("foncteur");
    for (int i = 0; ligne.at(foncteur + 8 + i) != ':'; ++i)
    {
        id_foncteur.push_back(ligne.at(foncteur + 8 + i));
    }
    id_foncteur.erase(remove(id_foncteur.begin(), id_foncteur.end(), ' '), id_foncteur.end());
    return id_foncteur;
}
/*
*Retourne l'identificateur du type de la ligne courante.
*
* @param ligne string,la ligne d'une declaration de type.
* @return id_type string,l'identificateur du type. 
*/
string identificateur_type(string const &ligne)
{
    string id_type;
    int type = ligne.find("type");
    for (int i = 0; ligne.at(type + 4 + i) != '='; ++i)
    {
        id_type.push_back(ligne.at(type + 4 + i));
    }
    id_type.erase(remove(id_type.begin(), id_type.end(), ' '), id_type.end());
    return id_type;
}
/*
*Retourne les arguments de notre type.
*
*@param ligne  string,la premiere ligne d'une déclaration de type.
*@return type_arguments string,les arguments de notre type. 
*/

string traitement_arguments_type(string const &ligne)
{
    string type_arguments;
    for (int i = (ligne.find('=') + 1); i < ligne.size(); ++i)
    {
        type_arguments.push_back(ligne.at(i));
    }
    type_arguments.erase(remove(type_arguments.begin(), type_arguments.end(), ' '), type_arguments.end());

    return type_arguments;
}