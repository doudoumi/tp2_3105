#ifndef TRAITEMENT_H
#define TRAITEMENT_H

#include <vector>
#include <string>
#include <iostream>
#include <vector>
#include <algorithm>
#include"arbremap.h"
using namespace std;
////////Fonction privé regardé le fichier .cpp//////////
void pre_traitement_line(vector<string> const &Fichier_lignes,ArbreMap<string,string> &type_dico,ArbreMap<string,vector<string> > &foncteur_dico);

////////Fonction privé regardé le fichier .cpp//////////
string identificateur_foncteur(string const &ligne);

////////Fonction privé regardé le fichier .cpp//////////
string identificateur_type(string const &ligne);

////////Fonction privé regardé le fichier .cpp//////////
string traitement_arguments_type(string const &ligne);


#endif
