#include "interaction.h"
/*
*Récupere les lignes de commande de l'utilisateur. 
*
*@param type_dico ArbreMap<string,string>,une structure de données qui utilise l'identificateur de type comme clé et une séquence d’identificateurs représentants les éléments possibles pour ce type comme élément.
*@param foncteur_dico ArbreMap<string,vector<string>>,une structure de données qui utilise l'identificateur du foncteur comme clé et ses closes comme arguments.
*/
void interaction(ArbreMap<string, string> &type_dico, ArbreMap< string, vector<string> > &foncteur_dico)
{
    string commande_ligne;
    while (getline(cin, commande_ligne))
    {
        commande_lignes_traitement(commande_ligne, type_dico, foncteur_dico);
    }
}
/*
*Traite les lignes de commande de l'utilisateur. 
*
*@param commande_ligne string,une référence vers la ligne de commande à traité.
*@param type_dico ArbreMap<string,string>,une structure de données qui utilise l'identificateur de type comme clé et une séquence d’identificateurs représentants les éléments possibles pour ce type comme élément.
*@param foncteur_dico ArbreMap<string,vector<string>>,une structure de données qui utilise l'identificateur du foncteur comme clé et ses closes comme arguments.
*/
void commande_lignes_traitement(string const &commande_ligne, ArbreMap<string, string> &type_dico, ArbreMap< string, vector<string> > &foncteur_dico)
{
    if (commande_ligne.find("(") == -1)
    {
        string id;
        for (int i = 0; commande_ligne.at(i) != '?'; ++i)
        {
            id.push_back(commande_ligne.at(i));
        }
        id.erase(remove(id.begin(), id.end(), ' '), id.end());
        if (type_dico.contient(id))
        {
            cout << type_dico[id] << endl;
        }
        if (foncteur_dico.contient(id))
        {
            for (int j = 0; j < foncteur_dico[id].size(); ++j)
            {
                cout << foncteur_dico[id].at(j) << endl;
            }
        }
    }
    else
    {
        string id_foncteur, test_gauche, test_droite;
        int position_argument;
        for (int k = 0; commande_ligne.at(k) != '('; ++k)
        {
            id_foncteur.push_back(commande_ligne.at(k));
        }
        id_foncteur.erase(remove(id_foncteur.begin(), id_foncteur.end(), ' '), id_foncteur.end());
        position_argument = traitement_variable(commande_ligne);
        traitement_close_foncteur(commande_ligne, foncteur_dico[id_foncteur], id_foncteur, position_argument);
    }
}
/*
*Compare les partie droite et gauche de toute les closes et affiche les possibilitées.
*
* @param ligne string,la premiere ligne d'une déclaration de foncteur.
* @param close_fonctor vector<string>,les closes du foncteur sur lequelle on travaille.
* @param id_foncteur string,identificateur du foncteur de la ligne de commande.
* @param position_variable entier,la position du point d'intérogation.
*/
void traitement_close_foncteur(string const &commande_ligne, vector<string> const &close_foncteur, string id_foncteur, int position_variable)
{
    vector<string> variables_possibles;
    string variable;
    int nombre_de_virgules = 0;

    for (int i = 0; i < close_foncteur.size(); ++i)
    {
        if ((partie_gauche(commande_ligne, position_variable, id_foncteur) == partie_gauche(close_foncteur.at(i), position_variable)) && (partie_droite(commande_ligne, position_variable) == partie_droite(close_foncteur.at(i), position_variable)))
        {
            for (int j = 0; (nombre_de_virgules < position_variable) && (j < close_foncteur.at(i).size()); ++j)
            {
                if (close_foncteur.at(i).at(j) == ',')
                {
                    ++nombre_de_virgules;
                }
                if (nombre_de_virgules == (position_variable - 1))
                {
                    variable.push_back(close_foncteur.at(i).at(j));
                }
            }
        }
        nombre_de_virgules = 0;
        variable.erase(remove(variable.begin(), variable.end(), ' '), variable.end());
        variable.erase(remove(variable.begin(), variable.end(), ','), variable.end());
        variable.erase(remove(variable.begin(), variable.end(), '('), variable.end());
        variable.erase(remove(variable.begin(), variable.end(), ')'), variable.end());
        if ((variable.size() != 0) && !(count(variables_possibles.begin(),variables_possibles.end(),variable)))
        {
            variables_possibles.push_back(variable);
        }
        variable.clear();
    }
    cout << '{';
    for (int k = 0; k < variables_possibles.size(); ++k)
    {
        cout << variables_possibles.at(k);
        if ((variables_possibles.size() > 1) && (k < (variables_possibles.size() - 1)))
            cout << ',';
    }
    cout << '}' << endl;
}
/*
*Retourne la partie gauche de la ligne donnée.
*
* @param ligne string,la premiere ligne d'une déclaration de foncteur.
* @param position_variable entier,la position du point d'intérogation.
* @param id_foncteur string,identificateur du foncteur de la ligne de commande.
*
* @return partie_gauche string,la partie gauche de la ligne donnée.
*/
string partie_gauche(string const &ligne, int position_variable, string id_foncteur)
{
    string partie_gauche;
    int nombre_arguments = 0;
    for (int i = ligne.find_first_not_of(id_foncteur); nombre_arguments < (position_variable - 1); ++i)
    {
        if (ligne.at(i) == ',')
        {
            ++nombre_arguments;
        }
        partie_gauche.push_back(ligne.at(i));
    }
    partie_gauche.erase(remove(partie_gauche.begin(), partie_gauche.end(), ' '), partie_gauche.end());

    return partie_gauche;
}
/*
*Retourne la partie gauche de la ligne donnée.
*
* @param ligne string,la premiere ligne d'une déclaration de foncteur.
* @param position_variable entier,la position du point d'intérogation.
*
* @return partie_gauche string,la partie gauche de la ligne donnée.
*/
string partie_gauche(string const &ligne, int position_variable)
{
    string partie_gauche;
    int nombre_arguments = 0;
    for (int i = 0; nombre_arguments < (position_variable - 1); ++i)
    {
        if (ligne.at(i) == ',')
        {
            ++nombre_arguments;
        }
        partie_gauche.push_back(ligne.at(i));
    }
    partie_gauche.erase(remove(partie_gauche.begin(), partie_gauche.end(), ' '), partie_gauche.end());

    return partie_gauche;
}
/*
*Retourne la partie droite de la ligne donnée.
*
* @param ligne string,la premiere ligne d'une déclaration de foncteur.
* @param position_variable entier,la position du point d'intérogation.
*
* @return partie_droite string,la partie droite de la ligne donnée.
*/
string partie_droite(string const &ligne, int position_variable)
{
    string partie_droite;
    int nombre_arguments = 0;
    for (int i = 0; i < ligne.size(); ++i)
    {
        if (ligne.at(i) == ',')
        {
            ++nombre_arguments;
        }
        if (nombre_arguments >= position_variable)
        {
            partie_droite.push_back(ligne.at(i));
        }
    }
    partie_droite.erase(remove(partie_droite.begin(), partie_droite.end(), ' '), partie_droite.end());
    return partie_droite;
}
/*
*Détermine la position de notre variable en comptant le nombre de virgules.
*
* @param ligne string,la premiere ligne d'une déclaration de foncteur.
*
* @return position_argument entier,le nombre d'éléments avant notre variable.
*/
int traitement_variable(string const &ligne)
{
    int position_argument = 1;
    for (int i = 0; ligne.at(i) != '?'; ++i)
    {
        if (ligne.at(i) == ',')
        {
            ++position_argument;
        }
    }
    return position_argument;
}