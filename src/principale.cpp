#include "principale.h"

#define bruno_malenfant main

/*
*Nom:BROURI,Adem
*Code permanent:BRO23089702
*
*
*/

using namespace std;

int bruno_malenfant(int argc, char *argv[])
{
    string Nom_fichier,Contenu_fichier;
    vector<string> Fichier_lignes;
    ArbreMap<string,string> type_dico;
    ArbreMap<string,vector<string> > foncteur_dico ;
    if(argc != 2){
        cerr << "ERREUR: Mauvaise Argument" << endl;
    }
    else{
    ifstream file;
    file.open(argv[1]);
    if(file.is_open()){
        while(!file.eof()){
        getline(file,Contenu_fichier);
        Fichier_lignes.push_back(Contenu_fichier);
        }
        file.close();
        pre_traitement_line(Fichier_lignes,type_dico,foncteur_dico);
        interaction(type_dico,foncteur_dico);
    }else{
        cerr << "ERREUR:Fichier non ouvert" << endl ;
    }
    }

  return 0;
}