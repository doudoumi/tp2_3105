/* UQAM / Département d'informatique
   INF3105 - Structures de données et algorithmes
   Squelette pour classe générique ArbreAVL<T> pour le Lab6 et le TP2.

   AUTEUR(S):
    BROURI Adem BROA23089702
Certaines de mes méthodes son inspiré des notes de cours d'éric beaudry http://ericbeaudry.ca/INF3105/diapos/arbres-binrech.pdf
*/

#if !defined(__ARBREAVL_H__)
#define __ARBREAVL_H__
#include <cassert>
#include "pile.h"

template <class T>
class ArbreAVL
{
  public:
    ArbreAVL();
    ArbreAVL(const ArbreAVL &);
    ~ArbreAVL();

    // Annonce l'existance d'une classe Iterateur.
    class Iterateur;

    void inserer(const T &);
    bool contient(const T &) const;
    bool vide() const;
    void vider();
    void enlever(const T &); // non requis pour le TP2.
    int hauteur() const;

    // Fonctions pour obtenir un itérateur (position dans l'arbre)
    Iterateur debut() const;
    Iterateur fin() const;
    Iterateur rechercher(const T &) const;
    Iterateur rechercherEgalOuSuivant(const T &) const;
    Iterateur rechercherEgalOuPrecedent(const T &) const;

    // Accès aux éléments de l'arbre via un intérateur.
    const T &operator[](const Iterateur &) const;
    T &operator[](const Iterateur &);

    // Copie d'un arbre AVL.
    ArbreAVL &operator=(const ArbreAVL &);

  private:
    struct Noeud
    {
        Noeud(const T &);
        T contenu;
        int equilibre;
        Noeud *gauche,
            *droite;
    };
    Noeud *racine;

    // Fonctions internes
    bool inserer(Noeud *&, const T &);
    void rotationGaucheDroite(Noeud *&);
    void rotationDroiteGauche(Noeud *&);
    void vider(Noeud *&);
    void copier(const Noeud *, Noeud *&) const;
    const T &max(Noeud *) const;
    bool enlever(Noeud *&, const T &e);
    int hauteur(const Noeud *n) const;

  public:
    // Sera présenté à la semaine #7
    class Iterateur
    {
      public:
        Iterateur(const ArbreAVL &a);
        Iterateur(const Iterateur &a);
        Iterateur(const ArbreAVL &a, Noeud *c);

        operator bool() const;
        bool operator!() const;
        bool operator==(const Iterateur &) const;
        bool operator!=(const Iterateur &) const;

        const T &operator*() const;

        Iterateur &operator++();
        Iterateur operator++(int);
        Iterateur &operator=(const Iterateur &);

      private:
        const ArbreAVL &arbre_associe;
        Noeud *courant;
        Pile<Noeud *> chemin;

        friend class ArbreAVL;
    };
};

//-----------------------------------------------------------------------------

template <class T>
ArbreAVL<T>::Noeud::Noeud(const T &c)
    : contenu(c), equilibre(0), gauche(NULL), droite(NULL)
{
}

template <class T>
ArbreAVL<T>::ArbreAVL()
    : racine(NULL)
{
}

template <class T>
ArbreAVL<T>::ArbreAVL(const ArbreAVL<T> &autre)
    : racine(NULL)
{
    copier(autre.racine, racine);
}

template <class T>
ArbreAVL<T>::~ArbreAVL()
{
    vider(racine);
}
/*
*Retourne true si notre arbre contient l'élément recherché sinon retourne false.
*
*@param element T,l'élément dont on verifie l'existence.
*@return retourne true si l'élément est contenue dans l'arbreavl false dans le cas contraire.
*/
template <class T>
bool ArbreAVL<T>::contient(const T &element) const
{
    Noeud *fouilleur = this->racine;

    while (fouilleur != NULL)
    {
        if (fouilleur->contenu < element)
        {
            fouilleur = fouilleur->droite;
        }
        else if (element < fouilleur->contenu)
        {
            fouilleur = fouilleur->gauche;
        }
        else
        {
            return true;
        }
    }
    return false;
}

template <class T>
void ArbreAVL<T>::inserer(const T &element)
{
    inserer(racine, element);
}
/*
*Insére un élément toute en maitenant l'équilibre de l'arbre avl.
*
*@param noeud Noeud,le noeud dans lequelle sera inserer l'élément.
*@param element T,l'élément que l'on soihaite inserer.
*@return retoune true si l'élément a bien été ajouter sinon retoune false
*
*/
template <class T>
bool ArbreAVL<T>::inserer(Noeud *&noeud, const T &element)
{
    if (noeud == NULL)
    {
        noeud = new Noeud(element);
        return true;
    }
    if (element < noeud->contenu)
    {
        if (inserer(noeud->gauche, element))
        {
            noeud->equilibre++;
            if (noeud->equilibre == 0)
                return false;
            if (noeud->equilibre == 1)
                return true;
            assert(noeud->equilibre == 2);
            if (noeud->gauche->equilibre == -1)
                rotationDroiteGauche(noeud->gauche);
            rotationGaucheDroite(noeud);
        }
        return false;
    }
    else if (noeud->contenu < element)
    {
        if (inserer(noeud->droite, element))
        {
            noeud->equilibre--;
            if (noeud->equilibre == 0)
                return false;
            if (noeud->equilibre == -1)
                return true;
            assert(noeud->equilibre == -2);
            if (noeud->droite->equilibre == 1)
                rotationGaucheDroite(noeud->droite);
            rotationDroiteGauche(noeud);
        }
        return false;
    }
    noeud->contenu = element;
    return false;
}

template <class T>
void ArbreAVL<T>::rotationGaucheDroite(Noeud *&racinesousarbre)
{
    Noeud *temp = racinesousarbre->gauche;
    int ea = temp->equilibre;
    int eb = racinesousarbre->equilibre;
    int neb = -(ea > 0 ? ea : 0) - 1 + eb;
    int nea = ea + (neb < 0 ? neb : 0) - 1;

    temp->equilibre = nea;
    racinesousarbre->equilibre = neb;
    racinesousarbre->gauche = temp->droite;
    temp->droite = racinesousarbre;
    racinesousarbre = temp;
}
/*
*Èffectue la rotation Droite gauche du noueud choisie.
*
*@param racinesousarbre le Noeud du sous arbre dans lequelle se fera la rotation.
*
*/
template <class T>
void ArbreAVL<T>::rotationDroiteGauche(Noeud *&racinesousarbre)
{
    Noeud *temp = racinesousarbre->droite;
    int ea = temp->equilibre;
    int eb = racinesousarbre->equilibre;
    int neb = eb + 1 - (ea < 0 ? ea : 0);
    int nea = 1 + ea + (neb > 0 ? neb : 0);

    temp->equilibre = nea;
    racinesousarbre->equilibre = neb;
    racinesousarbre->droite = temp->gauche;
    temp->gauche = racinesousarbre;
    racinesousarbre = temp;
}

template <class T>
bool ArbreAVL<T>::vide() const
{
    return NULL == racine;
}

template <class T>
void ArbreAVL<T>::vider()
{
    vider(racine);
}
/*
*Supprime un noeud avec tous ses enfants.
*
*@param noueud Noeud,le noeud ou commence la suppression.
*
*@return cas de base qui arréte notre fonction.
*/
template <class T>
void ArbreAVL<T>::vider(Noeud *&noeud)
{
    if (noeud == NULL)
        return;
    vider(noeud->gauche);
    vider(noeud->droite);
    delete noeud;
    noeud = NULL;
}
/*
*Copie un noeud et ses enfants.
*
*@param source Noeud,le noeud que l'on soihaite copié.
*@param noeud  Noeud,le nouveau noeud crée.
*/
template <class T>
void ArbreAVL<T>::copier(const Noeud *source, Noeud *&noeud) const
{
    if (source != NULL)
    {
        noeud = new Noeud(source->contenu);
        noeud->equilibre = source->equilibre;
        copier(source->gauche, noeud->gauche);
        copier(source->droite, noeud->droite);
    }
}

template <class T>
int ArbreAVL<T>::hauteur(const Noeud *n) const
{
    if (n == NULL)
        return 0;
    int hauteurg = hauteur(n->gauche);
    int hauteurd = hauteur(n->droite);
    return 1 + ((hauteurg > hauteurd) ? hauteurg : hauteurd);
}

template <class T>
int ArbreAVL<T>::hauteur() const
{
    return hauteur(racine);
}
/*
*Donne la plus grande valeur contenu dans le sous-arbre commencant a n.
*
*@param n le noeud ou commence notre sous-arbre.
*@return contenu la plus grande valeur trouver.
*/
template <class T>
const T &ArbreAVL<T>::max(Noeud *n) const
{
    assert(n != NULL);
    while (n->droite != NULL)
    {
        n = n->droite;
    }
    return n->contenu;
}

// L'enlèvement est optionnel (non requise pour le TP2)
template <class T>
void ArbreAVL<T>::enlever(const T &element)
{
    enlever(racine, element);
}

template <class T>
bool ArbreAVL<T>::enlever(Noeud *&noeud, const T &element)
{
    if (element < noeud->contenu)
    {
        if (enlever(noeud->gauche, element))
        {
            // ...
        }
    }
    else if (element > noeud->contenu)
    {
        // ...
    }
    else if (element == noeud->contenu)
    {
        if (noeud->gauche == NULL && noeud->droite == NULL)
        {
            delete noeud;
            noeud = NULL;
            return true;
        }
        else
        {
            // ...
            return true;
        }
    }
}
//----------- L'enlèvement est optionnel (non requise pour le TP2) ----------//
/*
*Empile les valeurs de l'arbre situé à gauche.
*
*@return iter Iterateur,un iterateur vers l'élément le plus petit de l'arbre.
*/
template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::debut() const
{
    Iterateur iter(*this);
    iter.courant = racine;
    if (iter.courant != NULL)
    {
        while (iter.courant->gauche != NULL)
        {
            iter.chemin.empiler(iter.courant);
            iter.courant = iter.courant->gauche;
        }
    }
    return iter;
}

template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::fin() const
{
    return Iterateur(*this);
}
/*
*Recherche et trouve un élément dans l'arbre.
*
*@param e T,l'élément que l'on recherche.
*
*@return iter Iterateur,un iterateur égale à l'élément recherché s'il éxiste.
*/
template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::rechercher(const T &e) const
{
    Iterateur iter(*this);
    Noeud *fouilleur = racine;
    while (fouilleur != NULL)
    {
        if (e < fouilleur->contenu)
        {
            iter.chemin.empiler(fouilleur);
            fouilleur = fouilleur->gauche;
        }
        else if (fouilleur->contenu < e)
        {
            fouilleur = fouilleur->droite;
        }
        else
        {
            iter.courant = fouilleur;
            return iter;
        }
    }
    iter.chemin.vider();
    return iter;
}
/*
*Recherche et trouve un élément dans l'arbre ou la valeur supérieur la plus proche.
*
*@param e T,l'élément que l'on recherche.
*
*@return iter Iterateur,un iterateur égale à l'élément recherché s'il éxiste ou à la valeur supérieur la plus proche.
*/
template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::rechercherEgalOuSuivant(const T &e) const
{
    Noeud *fouilleur = racine;
    Noeud *dernier = NULL;
    while (fouilleur != NULL)
    {
        if (e < fouilleur->contenu)
        {
            dernier = fouilleur;
            fouilleur = fouilleur->gauche;
        }
        else if (fouilleur->contenu < e)
        {
            fouilleur = fouilleur->droite;
        }
        else
        {
            return rechercher(e);
        }
    }
    if (dernier != NULL)
    {
        return rechercher(dernier->contenu);
    }
    return Iterateur(*this);
}
/*
*Recherche et trouve un élément dans l'arbre ou la valeur inférieur la plus proche.
*
*@param e T,l'élément que l'on recherche.
*
*@return iter Iterateur,un iterateur égale à l'élément recherché s'il éxiste ou à la valeur inférieur la plus proche.
*/
template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::rechercherEgalOuPrecedent(const T &e) const
{
    Noeud *fouilleur = racine;
    Noeud *dernier = NULL;
    while (fouilleur != NULL)
    {
        if (e < fouilleur->contenu)
        {
            fouilleur = fouilleur->gauche;
        }
        else if (fouilleur->contenu < e)
        {
            dernier = fouilleur;
            fouilleur = fouilleur->droite;
        }
        else
        {
            return rechercher(e);
        }
    }
    if (dernier != NULL)
    {
        return rechercher(dernier->contenu);
    }
    return Iterateur(*this);
}

template <class T>
const T &ArbreAVL<T>::operator[](const Iterateur &iterateur) const
{
    assert(&iterateur.arbre_associe == this);
    assert(iterateur.courant);
    return iterateur.courant->contenu;
}

template <class T>
T &ArbreAVL<T>::operator[](const Iterateur &iterateur)
{
    assert(&iterateur.arbre_associe == this);
    assert(iterateur.courant);
    return iterateur.courant->contenu;
}

template <class T>
ArbreAVL<T> &ArbreAVL<T>::operator=(const ArbreAVL &autre)
{
    if (this == &autre)
        return *this;
    vider();
    copier(autre.racine, racine);
    return *this;
}

//-----------------------
template <class T>
ArbreAVL<T>::Iterateur::Iterateur(const ArbreAVL &a)
    : arbre_associe(a), courant(NULL)
{
}

template <class T>
ArbreAVL<T>::Iterateur::Iterateur(const ArbreAVL<T>::Iterateur &a)
    : arbre_associe(a.arbre_associe)
{
    courant = a.courant;
    chemin = a.chemin;
}

// Pré-incrément
/*
*Surcharge l'operateur ++ pour permettre a l'iterateur de s'incremente.
*
*@return un pointeur sur l'instance de l'objet sur lequelle on travaille.
*/
template <class T>
typename ArbreAVL<T>::Iterateur &ArbreAVL<T>::Iterateur::operator++()
{
    assert(courant != NULL);
    Noeud *suivant = courant->droite;
    while (suivant != NULL)
    {
        chemin.empiler(suivant);
        suivant = suivant->gauche;
    }
    if (!chemin.vide())
        chemin.depiler(courant);
    else
        courant = NULL;
    return *this;
}

// Post-incrément
template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::Iterateur::operator++(int)
{
    Iterateur copie(*this);
    operator++();
    return copie;
}

template <class T>
ArbreAVL<T>::Iterateur::operator bool() const
{
    return courant != NULL;
}

template <class T>
bool ArbreAVL<T>::Iterateur::operator!() const
{
    return courant == NULL;
}

template <class T>
bool ArbreAVL<T>::Iterateur::operator==(const Iterateur &o) const
{
    assert(&arbre_associe == &o.arbre_associe);
    return courant == o.courant;
}

template <class T>
bool ArbreAVL<T>::Iterateur::operator!=(const Iterateur &o) const
{
    assert(&arbre_associe == &o.arbre_associe);
    return courant != o.courant;
}

template <class T>
const T &ArbreAVL<T>::Iterateur::operator*() const
{
    assert(courant != NULL);
    return courant->contenu;
}

template <class T>
typename ArbreAVL<T>::Iterateur &ArbreAVL<T>::Iterateur::operator=(const Iterateur &autre)
{
    assert(&arbre_associe == &autre.arbre_associe);
    courant = autre.courant;
    chemin = autre.chemin;
    return *this;
}

#endif
