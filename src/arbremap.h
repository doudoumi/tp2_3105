/* UQAM / Département d'informatique
   INF3105 - Structures de données et algorithmes
   Squelette pour classe générique ArbreMap<K,V> pour le Lab8 et le TP2.

   AUTEUR(S):
    BROURI Adem BROA23089702
	Toute les fonctions d'arbremap ne sont que des appelles à celle d'arbreavl.
    Note de cours d'éric Beaudry http://ericbeaudry.ca/INF3105/diapos/arbremap.pdf
*/

#if !defined(__ARBREMAP_H__)
#define __ARBREMAP_H__

#include "arbreavl.h"

template <class K, class V>
class ArbreMap
{
class Entree{
 		public:
 			Entree(const K& c):cle(c),valeur(){}
 			K cle;
 			V valeur;
 			bool operator < (const Entree& e) const { return cle < e.cle; }
			
 	};
  private:
    
    ArbreAVL<Entree> entrees;

  public:
  class Iterateur {
  	public:
		Iterateur(ArbreMap& a) : iter(a.entrees.debut()) {}
		Iterateur(typename ArbreAVL<Entree>::Iterateur i) : iter(i) {}
		operator bool() const {return iter.operator bool();};
		Iterateur& operator++() {iter++; return *this;}
		 bool operator==(const Iterateur& o) const {return iter == o.iter;} ;
        bool operator!=(const Iterateur& o) const {return iter != o.iter;};
		const K& cle() const {return (*iter).cle;}
		V& valeur() {return (V&) (*iter).valeur;}
		V	valeur_() {return (V) (*iter).valeur;}
	private:
		typename ArbreAVL<Entree>::Iterateur iter;
		};
	Iterateur debut() { return Iterateur(*this); }
	Iterateur fin() { return Iterateur(entrees.fin());}
	Iterateur rechercher(const K& cle) { return Iterateur(entrees.rechercher(cle));}
	Iterateur rechercherEgalOuSuivant(const K& cle) {return
	Iterateur(entrees.rechercherEgalOuSuivant(cle));}
	Iterateur rechercherEgalOuPrecedent(const K& cle) {return
	Iterateur(entrees.rechercherEgalOuPrecedent(cle));}

    bool contient(const K&) const;

    void enlever(const K&);
    void vider();
    bool vide() const;
	ArbreMap(const ArbreMap<K,V>&);
	ArbreMap(){};
    const V& operator[] (const K&) const;
    V& operator[] (const K&);
};

template <class K, class V>
ArbreMap<K,V>::ArbreMap(const ArbreMap<K,V>& a){
	entrees = a.entrees;
	
}
	
template <class K, class V>
void ArbreMap<K,V>::vider(){
    
    entrees.vider();
}

template <class K, class V>
bool ArbreMap<K,V>::vide() const{
    
    return entrees.vide();
   
}

template <class K, class V>
void ArbreMap<K,V>::enlever(const K& c)
{
    // À compléter
}

template <class K, class V>
bool ArbreMap<K,V>::contient(const K& cle) const
{
return entrees.contient(cle);
}

template <class K, class V>
 const V& ArbreMap<K,V>::operator[] (const K& c) const {
 typename ArbreAVL<Entree>::Iterateur iter=entrees.rechercher(c);
 return entrees[iter].valeur;
 }

template <class K, class V>
 V& ArbreMap<K,V>::operator[] (const K& c) {
 typename ArbreAVL<Entree>::Iterateur iter=entrees.rechercher(c);
 if(!iter){
 entrees.inserer(Entree(c));
 iter = entrees.rechercher(c);
 }
 return entrees[iter].valeur;
 }


#endif

